package br.com.mastertech.imersivo.estacionamento.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Carro sem entrada!")
public class CarroSemEntradaException extends RuntimeException {

}
